import pyslackline as ps
import pandas as pd
import matplotlib.pyplot as plt

if __name__ == "__main__":
    plt.style.use('mjr_dark.mplstyle')

    # delete boring beginning
    sc = ps.SlackCell('tests/integration/fixtures/SlackCellForce.txt')
    start = 2 * 60 - .05
    # 250
    end = 250
    sc.set_start(start)
    sc.set_end(end)
    sc.plots()
    # sc.print()
    sc.json()
    sc.simple_static_sag_model(80, 65)
    ps.plot.sag(sc.df)
